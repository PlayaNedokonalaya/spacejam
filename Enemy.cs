using Godot;
using System;

public class Enemy : RigidBody2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    [Export] public int Health;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Health = 200;
    }


    public override void _PhysicsProcess(float delta) 
    {
        if (this.Health <= 0) 
        {
            this.QueueFree();
        }
    }

    public void _RecieveDamage(int damage)
    {
        Health -= damage;
    }

}
