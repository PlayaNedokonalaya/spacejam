using Godot;
using System;

public class Arrow : RigidBody2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    [Export] public int Damage;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Damage = 22;
        Connect("body_entered", this, nameof(this._onCollision));
    }

    public override void _PhysicsProcess(float delta)
    {
       
    }

    public void _onCollision(Node body) 
    {
        if (body is Enemy)
        {
            var enemy = body as Enemy;
            enemy._RecieveDamage(Damage);
            this.QueueFree();
        }
        
        if (body is DeathStar)
        {
            var enemy = body as DeathStar;
            enemy._RecieveDamage(Damage);
            this.QueueFree();
        }

        if (body is SpaceShip)
        {
            var enemy = body as SpaceShip;
            enemy._RecieveDamage(Damage);
            this.QueueFree();
        }
    }
}
