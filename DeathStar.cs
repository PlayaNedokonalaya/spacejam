using Godot;
using System;

public class DeathStar : RigidBody2D
{
    [Export] public int Health;
    [Export] public PackedScene Ammunition;
    [Export] public float ShootVelocity = 250;
    private Node2D[] WeaponPos = new Node2D[4];
    
    public override void _Ready()
    {
        if (Ammunition == null)
        {
            Ammunition = GD.Load<PackedScene>("res://Arrow.tscn");
        }

        for(int i = 1; i < 5; i++)
        {
            WeaponPos[i - 1] = GetNode<Node2D>("Node2D" + i);
        }

        Health = 300;

        var Timer = GetNode<Timer>("Timer");
        Timer.Connect("timeout", this, nameof(this._Shooting));
    }


    public override void _PhysicsProcess(float delta) 
    {
        if (this.Health <= 0) 
        {
            this.QueueFree();
        }


    }

    public void _Shooting()
    {
        for(int i = 1; i < 5; i++)
        {
            var Bullet = Ammunition.Instance() as RigidBody2D;
            Bullet.Rotation = WeaponPos[i-1].Rotation;
            Bullet.Position = WeaponPos[i-1].GetGlobalPosition();
            Bullet.ApplyCentralImpulse(new Vector2(Mathf.Sin(WeaponPos[i-1].Rotation)*ShootVelocity, -Mathf.Cos(WeaponPos[i-1].Rotation)*ShootVelocity));
            GetNode("/root/GameWorld").AddChild(Bullet);
        }
    }

    public void _RecieveDamage(int damage)
    {
        Health -= damage;
    }
}
