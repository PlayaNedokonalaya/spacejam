using Godot;
using System;

public class SpaceShip : RigidBody2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    [Export] public float Speed = 20;
    [Export] public int Health;
    [Export] public PackedScene Ammunition;
    [Export] public float ShootVelocity = 250;
    // Called when the node enters the scene tree for the first time.

    private Node2D WeaponPos;
    public override void _Ready()
    {
        if (Ammunition == null)
        {
            Ammunition = GD.Load<PackedScene>("res://Arrow.tscn");
        }

        Health = 150;
        
        WeaponPos = GetNode<Node2D>("WeaponPos");
    }

    public override void _PhysicsProcess(float delta)
    {
        if (Input.IsActionPressed("left"))
        {
            this.ApplyTorqueImpulse(-Speed*5);
        }
         
        if (Input.IsActionPressed("right"))
        {
            this.ApplyTorqueImpulse(Speed*5);
        }
        
        if (Input.IsActionPressed("up"))
        {
            this.ApplyImpulse(Vector2.Zero, new Vector2(Mathf.Sin(this.Rotation)*Speed, -Mathf.Cos(this.Rotation)*Speed));
        }
        
        if (Input.IsActionPressed("down"))
        {
            this.ApplyImpulse(Vector2.Zero, new Vector2(-Mathf.Sin(this.Rotation)*Speed, Mathf.Cos(this.Rotation)*Speed));
        }

        if (Input.IsActionPressed("strafeLeft"))
        {
            this.ApplyImpulse(Vector2.Zero, new Vector2(Mathf.Sin(this.Rotation - Mathf.Pi/2)*Speed, -Mathf.Cos(this.Rotation - Mathf.Pi/2)*Speed));
        }
        
        if (Input.IsActionPressed("strafeRight"))
        {
            this.ApplyImpulse(Vector2.Zero, new Vector2(-Mathf.Sin(this.Rotation - Mathf.Pi/2)*Speed, Mathf.Cos(this.Rotation - Mathf.Pi/2)*Speed));
        }

        if (Input.IsActionJustPressed("shoot"))
        {
            _Shooting(ShootVelocity);
        }

        if (Input.IsActionJustPressed("use"))
        {
            _Leave();
        }

        if (this.Health <= 0) 
        {
            Console.WriteLine("GAME OVER");
            this.QueueFree();
        }
    }

    public void _Shooting (float velocity)
    {
        var Bullet = Ammunition.Instance() as RigidBody2D;
        Bullet.Rotation = this.Rotation;
        Bullet.Position = WeaponPos.GetGlobalPosition();
        Bullet.ApplyCentralImpulse(new Vector2(Mathf.Sin(this.Rotation)*ShootVelocity, -Mathf.Cos(this.Rotation)*ShootVelocity));
        GetNode("/root/GameWorld").AddChild(Bullet);
    }

    public void _Leave()
    {
        // leave the ship, then program script for Pilot
    }

    public void _RecieveDamage(int damage)
    {
        Health -= damage;
    }
}
